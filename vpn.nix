{ config, lib, pkgs, ... }:

let
  wireguardPrefix = "240b:10:2ea0:c700:f00";
in
{
  networking.wireguard.interfaces = {
    wg0 = {
      ips = [
        "10.88.0.42/32"
        "${wireguardPrefix}::2/128"
      ];

      privateKeyFile = "/etc/nixos/wireguard-private-key";

      peers = [{
        publicKey = "8yv8ENDNlTrabMP2ZcrfyMMYGopacUZY6aWCQtJOjQI=";
        allowedIPs = [
          "0.0.0.0/0"
          "::/0"
        ];
        endpoint = "106.72.46.160:52336";
        persistentKeepalive = 25;
      }];

      allowedIPsAsRoutes = false;

      postSetup = ''
        ip route replace 10.88/16 dev wg0
        ip route replace ::/0     dev wg0
      '';
    };
  };

  # Don't enable by default, for now.
  # I'm in the land that hasn't heard of ipv6.
  # systemd.services.wireguard-wg0.wantedBy = lib.mkForce [];
}
