{ config, pkgs, ... }:

let
  checkout_dir = "/home/lorne/src/eikaiwa_content_frontend";
in
{
  services.nginx = {
    enable = true;
    virtualHosts.default = {
      default = true;
      forceSSL = true;
      globalRedirect = "localhost.devdomain.name:3000";
      sslCertificate = "${checkout_dir}/config/dev-cert.pem";
      sslCertificateKey = "${checkout_dir}/config/dev-key.pem";
    };
  };
}
