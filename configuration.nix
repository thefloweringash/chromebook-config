# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, lib, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
      ./lorne-nix
      ./vpn.nix
      ./eikaiwa-redirect.nix
      <kevin-nix/modules>
    ];

  nixpkgs.overlays = [
    (import ./overlays/aarch64-fixes.nix)
  ];

  boot.loader.grub.enable = false;
  boot.loader.depthcharge.enable = true;
  boot.loader.depthcharge.partition = "/dev/disk/by-partlabel/NIX-KERN-A";
  boot.plymouth.enable = true;

  boot.cleanTmpDir = true;

  networking.hostName = "kevin";
  # networking.networkmanager.unmanaged = [ "eth0" "eth1" ];

  time.timeZone = "Asia/Tokyo";

  environment.systemPackages = with pkgs; [
    ag
    albert
    apg
    avrdude
    awscli
    borgbackup
    colordiff
    diffstat
    ed
    file
    firefox
    fzf
    gdb
    gitAndTools.diff-so-fancy
    gitFull
    gnome3.eog
    gnupg
    gocryptfs
    iperf
    iw
    jq
    kakoune
    lshw
    ncdu
    nix-prefetch-scripts
    pv
    python2
    rclone
    ruby
    rxvt_unicode
    sakura
    surfraw
    sysstat
    tcpdump
    tcptraceroute
    tig
    tmux
    traceroute
    tree
    unzip
    usbutils
    vboot_reference
    vim
    wireshark
    xournal
    zeal
  ];

  # Make Firefox behave properly with touchscreens
  # TODO: Localized to the firefox package and share
  environment.variables.MOZ_USE_XINPUT2 = "1";

  services.openssh.enable = true;
  services.openssh.passwordAuthentication = false;

  # This "works", but has a strange movement inertia problem. There's a massive
  # difference between moving-and-stopped vs moving-and-lifting. When
  # moving-and-stopping, the cursor drifts, when moving-and-lifting it stops
  # immediately, as expected.
  services.xserver.libinput = {
    enable = false;
    accelProfile = "flat";
    accelSpeed = "1";
    naturalScrolling = true;
    clickMethod = "clickfinger";
  };

  # This doesn't seem to work. The driver is enabled, but only with defaults.
  # Remember PC laptops in the 90s? Yeah, that bad. Be glad everyone started
  # copying Apple.
  services.xserver.synaptics = {
    enable = false;
    twoFingerScroll = true;
    tapButtons = false;
    palmDetect = true;
    horizontalScroll = true;
    additionalOptions = ''
      Option "VertScollDelta" "-111"
      Option "HorizScollDelta" "-111"
    '';
  };

  boot.kernelParams = [ "console=tty1" ];

  zramSwap.enable = true; # Kevin's a little light on RAM

  # Screen is too high dpi to use normal console font
  # and we need to look at the screen to see the luks prompt
  hardware.kevin.console-font.fontfile =
    "${pkgs.source-code-pro}/share/fonts/opentype/SourceCodePro-Regular.otf";

  programs.zsh.enable = true;
  programs.mosh.enable = true;
  programs.light.enable = true;

  users.extraUsers.lorne = {
    isNormalUser = true;
    uid = 1000;
    # `video` is required for the `light` program to work. (TODO: why
    # is this not uaccess tagged?)
    extraGroups = [ "wheel" "networkmanager" "video" ];
    shell = pkgs.zsh;
  };

  # This value determines the NixOS release with which your system is to be
  # compatible, in order to avoid breaking some software such as database
  # servers. You should change this only after NixOS release notes say you
  # should.
  system.stateVersion = "18.03"; # Did you read the comment?

  boot.kernelPackages = pkgs.linuxPackages_latest;

  nix.binaryCaches = [
    "https://cache.nixos.org"
    "https://s3-ap-northeast-1.amazonaws.com/hydra.cons.org.nz/cache"
  ];

  nix.binaryCachePublicKeys = [
    "barbara.cons.org.nz-1:HplcOsCv3cNAZhCcqT+y9JfhN7DK5lvjc2CHZ1ajfUs="
  ];

  nix.trustedBinaryCaches = [
    "https://hydra.barbara.cons.org.nz"
  ];

  nix.useSandbox = true;

  # Optimise for fast cpu and slow storage
  nix.buildCores = 0;
  nix.maxJobs = 1;

  # Expose the accelerometers
  services.udev.extraRules = ''
    ACTION!="remove", ATTR{name}=="cros-ec-accel*", SYMLINK+="cros-ec-accel/%n", TAG+="uaccess"
  '';

  services.xserver.exportConfiguration = true;

  services.xserver.videoDrivers = [ "modesetting" ];

  services.xserver.xrandrHeads = [
    {
      output = "eDP-1";
      primary = true;
      monitorConfig = ''
        DisplaySize 317 211
      '';
    }
  ];

  services.xserver.displayManager.slim = {
    defaultUser = "lorne";
    autoLogin = true;
  };

  # Behave like a modern laptop
  services.logind.extraConfig = ''
    IdleAction=suspend
    IdleActionSec=60
    # Let's go with poweroff for now
    # in case of emergency
    # HandlePowerKey=suspend
  '';

  hardware.kevin.cmt.enable = true;

  lorne-nix = {
    desktop.enable = true;
    emacs.enable   = true;
    network.enable = true;
  };
}
