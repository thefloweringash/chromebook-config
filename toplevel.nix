with (import <nixpkgs/nixos> {
  configuration = ./configuration.nix;
});

{
  inherit (config.system.build)
    toplevel
    kernel
    ;

  inherit (pkgs)
    chromium
    firefox
    xournal
    xournalpp
    wireshark-qt
    avrdude
    zeal
    ;

  inherit (pkgs.gnome3)
    eog;
}
